$(document).ready(function() {

	//ho tro truc tuyen
	$('.support .title').click(function(event) {
		$('.support .noidung').toggleClass('block');
		$('.support .title').toggleClass('border');
		
	});
 

	$('#owl-demo').owlCarousel({
	    loop:true,
	    margin:10,
	    responsiveClass:true,
	    responsive:{
	        0:{
	            items:2,
	            nav:true
	        },
	        600:{
	            items:3,
	            nav:false
	        },
	        1000:{
	            items:8,
	            nav:true,
	            loop:false
	        }
	    }
	});


	$("#back-top").hide();
	// fade in #back-top
	$(function () {
		$(window).scroll(function () {
			if ($(this).scrollTop() > 100) {
				$('#back-top').fadeIn();
			} else {
				$('#back-top').fadeOut();
			}
		});

		// scroll body to 0px on click
		$('#back-top').click(function () {
			$('body,html').animate({
				scrollTop: 0
			}, 800);
			return false;
		});
	});

 
});